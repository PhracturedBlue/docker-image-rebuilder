import abc
import asyncio
import pathlib
import tarfile


class ParserStreamFeedWrapper:
	def __init__(self, stream, coroutine):
		"""
		Parameters
		----------
		stream : asyncio.StreamReader
			AsyncIO data buffer manager
		"""
		if hasattr(coroutine, "__await__"):
			coroutine = coroutine.__await__()
		elif hasattr(coroutine, "__iter__"):
			coroutine = coroutine.__iter__()
		
		self._stream = stream
		self._coro   = coroutine
	
	def feed_data(self, data):
		self._stream.feed_data(data)
		
		if hasattr(self, '_result'):
			raise BrokenPipeError("Coroutine has stopped accepting data")
		
		try:
			next(self._coro)
		except StopIteration as result:
			self._result = result.value
			return False
		return True
	
	def feed_eof(self):
		if not hasattr(self, '_result'):
			self._stream.feed_eof()
		
			try:
				while next(self._coro):
					self._stream.feed_eof()
			except StopIteration as result:
				self._result = result.value
		
		return self._result
	
	def at_eof(self):
		return self._stream.at_eof()


class TarParser(metaclass=abc.ABCMeta):
	def __init__(self, encoding=tarfile.ENCODING, errors="surrogateescape"):
		self.encoding = encoding
		self.errors   = errors
	
	
	async def parse(self, stream: asyncio.StreamReader):
		self.on_stream_start()
		
		while True:
			block = await stream.readexactly(512)
			
			# Parse TAR member header
			try:
				header = tarfile.TarInfo.frombuf(block, self.encoding, self.errors)
				self.on_member_header(header)
			except tarfile.EOFHeaderError:
				return self.on_stream_end()
			
			# Process all full content blocks
			for _ in range(header.size >> 9):
				self.on_member_content_block(await stream.readexactly(512))
			
			# Process last (partial) content block
			block_size_last = header.size & 0x1FF
			if block_size_last > 0:
				block = await stream.readexactly(512)
				self.on_member_content_block(block[0:block_size_last])
			
			self.on_member_end()
	
	def parse_feedingly(self):
		stream = asyncio.StreamReader()
		parser = self.parse(stream)
		return ParserStreamFeedWrapper(stream, parser)
	
	
	@abc.abstractmethod
	def on_member_header(self, header):
		pass
	
	@abc.abstractmethod
	def on_member_content_block(self, block):
		pass
	
	@abc.abstractmethod
	def on_member_end(self):
		pass
	
	def on_stream_start(self):
		pass
	
	def on_stream_end(self):
		pass